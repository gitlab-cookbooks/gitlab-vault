#
# Cookbook Name:: gitlab-vault
# Recipe:: default
#
# Copyright (C) 2016 GitLab Inc.
#
# MIT license
#
include_recipe 'chef-vault'
